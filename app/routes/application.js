import Ember from 'ember';

var MainMenu = Ember.Object.extend({
  topLevel: null,
});

export default Ember.Route.extend({
  model: function(){
    return MainMenu.create({
      topLevel: [
        {
          title: 'plants',
          target: 'plants',
          items: 'plants'
        }
      ]
    });
  }
});
