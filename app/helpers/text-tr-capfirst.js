import Ember from 'ember';

export function textTrCapfirst(input) {
  return typeof input === 'string' ? input.charAt(0).toUpperCase()+input.slice(1) : '' ;
}

export default Ember.Handlebars.makeBoundHelper(textTrCapfirst);
