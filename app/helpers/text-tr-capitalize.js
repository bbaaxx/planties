import Ember from 'ember';

export function textTrCapitalize(input) {
  return typeof input === 'string' ? input.toUpperCase() : '' ;
}

export default Ember.Handlebars.makeBoundHelper(textTrCapitalize);
